<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Jobs\MyEmailJob;

class EmailController extends Controller
{
    public function sendEmail()
    {
        $details = [
            'title' => 'Mail from Vipin',
            'body' => 'This is for testing email using smtp'
        ];
        
        
        // \Mail::to('sfs.vipin21@gmail.com')->send(new \App\Mail\MyTestMail($details));

        dispatch(new MyEmailJob($details));

     
        
        dd("Email is Sent.");
        
    }
}
